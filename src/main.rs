use rand::prelude::*;

fn main() {
    let mut rng = thread_rng();

    // Creating all Cards
    let mut cards = gen_cards(6,&mut rng);
    // Dealing cards & Creating the Players
    let players = get_players(&mut cards, 3, &mut rng);

    println!("{:#?}", players);
}

fn get_players<'a>(cards:&'a mut Vec<Card>, num_of_players:u32, rng:&'a mut ThreadRng) -> Vec<Deck<&'a Card>> {

    let card_per_person: usize = cards.len() / num_of_players as usize;

    let mut players: Vec<Deck<&Card>> = Vec::new();
    for i in 0..num_of_players {
        players.push(Deck {
            deck_type: "Bob ".to_owned() + &i.to_string(),
            cards: cards.choose_multiple(rng, card_per_person).collect(),
            // As cards are randomly chosen, there is no need to shuffle before hand.
        })
    }

    players
}

fn gen_cards(num_of_cards:u32, rng: &mut ThreadRng) -> Vec<Card>{
    let mut cards:Vec<Card> = Vec::new();
    for i in 0..num_of_cards {
        cards.push(Card {
            intelligence: rng.gen_range(0..10),
            skill: rng.gen_range(0..10),
            brain: rng.gen_range(0..10),
            name: i.to_string(),
        })
    }

    cards
}

#[derive(Debug)]
struct Deck<T> {
    deck_type: String,
    cards: Vec<T>,
}

#[derive(Debug)]
struct Card {
    name: String,
    intelligence: i32,
    skill: i32,
    brain: i32,
}
